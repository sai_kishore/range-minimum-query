# Range minimum query using OpenCv and Pygame

<p> Overview:

Recognizes an array of comma seperated numbers from the frame using Opencv
Calculates the Segment tree and Allows the user to Perform Range Minimum query
Displays the Animation of the segment tree using Pygame </p>


## Installation
```	sh 
	pip install -r requirements.txt'
```
## Execution
<p>
	1) Setup a desk over which your webcam is placed <br>
	2) install the required packages <br>
	3) Place a white A4 paper as baground <br> 
	4) Write a list of numbers using a thick marker -(example) :  5,2,3,4,5  <br>
	5) Make sure you have Rmq.py and model_mnist3.h5 in the same directory as 
		"Rmq_main.py"  <br>
	6) Run   <br>
	7) Press c to capture the next Frame incase the prediction is not right   <br>
	8) If you dont get the desired prediction after 3,4 frames, press t   <br>
	9) By presseing t you can change threshold   <br>
	10) Once everything is set, Press y for displaying the result and   <br>
		watching the animation of the segment tree   <br>
	11) After animation Press a key to close the pygame window   <br>
	12) You can write another set of numbers and try again or press q to exit   <br>
	</p>

## Functions Used
### Mnist 
 ```python 
	from keras.models import Sequential
	from keras.layers import Convolution2D, MaxPooling2D
	from keras.layers import Flatten, Dense

	def create_model():
		model = Sequential()
		model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(28,28, 3)))
		model.add(MaxPooling2D(2, 2))

		model.add(Convolution2D(32, 5, 5, activation='relu'))
		model.add(MaxPooling2D(2, 2))

		model.add(Flatten())
		model.add(Dense(1000, activation='relu'))

		model.add(Dense(11, activation='softmax'))
		return model

	model = create_model()
	model.load_weights('model_mnist3.h5')
```

### Segment Tree
```python

	def construct_segment_tree(segtree, a, n):   
	    # assign values to leaves of  
	    # the segment tree  
	    for i in range(n):  
	        segtree[n + i] = a[i];  
	      
	    # assign values to remaining nodes  
	    # to compute minimum in a given range  
	    for i in range(n - 1, 0, -1):  
	        segtree[i] = min(segtree[2 * i],  
	                         segtree[2 * i + 1])  

```
<p> Constructs a Segment tree for the given array. 
		left element is 2i, right element is 2i+1	
		Time Complexity is O(logn)</p>
		
### RangeQuery 
``` python
	def range_query(segtree, left, right, n): 
	''' Finds the minimum value between a given 
		range'''
	    left += n  
	    right += n 
	    mi = 1e9 # initialize minimum to a very high value 
	    while (left < right): 
	        if (left & 1): # if left index in odd  
	                mi = min(mi, segtree[left]) 
	                left = left + 1
	        if (right & 1): # if right index in odd  
	                right -= 1
	                mi = min(mi, segtree[right]) 
	        left = left // 2
	        right = right // 2
	    return mi

```

